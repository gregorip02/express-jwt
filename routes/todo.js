const Todo = require('../models/todo')
const todoRequest = require('../requests/todo')
const authMiddleware = require('../middleware/auth')

module.exports = router => {
  // Protege todas estas rutas contra acceso no permitido
  // @param {Function} authMiddleware
  router.all(/^\/todo*/, authMiddleware)

  // Envía la lista de todos de un usuario
  // @return {JSON}
  router.get('/todos', (req, res) => {
    const author = req.user._id
    Todo.find({ author }).populate('author').sort({ createdAt: -1 }).then(todos => {
      res.json(todos)
    })
  })

  // Obtiene detalles sobre un 'todo'
  // @return {JSON}
  router.get('/todo/:id', todoRequest.show, (req, res) => {
    const _id = req.params.id
    const author = req.user._id
    Todo.findOne({ _id, author }).populate('author').then(todo => {
      res.json(todo)
    }).catch(err => {
      console.log(err)
      res.json({})
    })
  })

  // Crea un nuevo 'todo'
  // @return {JSON}
  router.post('/todos', todoRequest.store, (req, res) => {
    const todo = Object.assign({}, { author: req.user._id }, req.body)
    Todo.create(todo).then(todo => {
      res.created(todo)
    }).catch(err => res.error(err))
  })

  // Actualiza un 'todo'
  // @return {JSON}
  router.put('/todo/:id', todoRequest.update, (req, res) => {
    const _id = req.params.id
    const author = req.user._id
    Todo.findOneAndUpdate({ _id, author }, req.body).then(todo => {
      res.created(todo)
    })
  })

  // Elimina un 'todo'
  // @return {JSON}
  router.delete('/todo/:id', todoRequest.show, (req, res) => {
    const _id = req.params.id
    const author = req.user._id
    Todo.deleteOne({ _id, author }).then(() => {
      res.sendStatus(204)
    }).catch(err => {
      console.log(err)
      res.error(err)
    })
  })
}
