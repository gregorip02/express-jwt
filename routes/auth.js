const User = require('../models/user')
const authRequest = require('../requests/auth')

module.exports = router => {
  // Controlador de autenticación
  router.post('/login', authRequest.login, async (req, res) => {
    const user = await User.findOne({ email: req.body.email })
    if (user) {
      user.login(req.body.password).then(token => {
        res.json({ user, token })
      }).catch(() => {
        // El usuario esta registrado, pero la contraseña que esta
        // enviando no es correcta.
        res.status(401).json({ passed: false, message: 'Contraseña incorrecta' })
      })
    } else {
      // El usuario no esta registrado.
      res.status(404).json({ passed: false, message: 'Este correo electrónico no existe' })
    }
  })

  // Controlador de registro
  router.post('/register', authRequest.register, (req, res) => {
    User.create(req.body).then(user => {
      user.login(req.body.password).then(token => {
        res.created({ user, token })
      })
    }).catch(err => res.error(err))
  })
}
