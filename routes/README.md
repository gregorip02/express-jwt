# Routes

Las rutas, desde mi punto de vista y de este proyecto: controladores. Son puntos
finales de las peticiones HTTP. Aquí se escribe toda la lógica para tomar decisiones
sobre la respuesta que sera enviada al cliente.

## Implementación

Cree las rutas que sean necesarias en este directorio, luego en el archivo `index.js`
registre los cambios.
