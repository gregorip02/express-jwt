const express = require('express')
const router = express.Router()

const authRoutes = require('./auth')
const todoRoutes = require('./todo')

/**
 * Funciones para respuestas automatizadas.
 *
 * @param {IncomingMessage} req
 * @param {ServerResponse} res
 * @param {Function} next
 *
 * @return Function next
 */
router.use((req, res, next) => {
  /**
   * Para peticiones erroneas
   *
   * @param  {Error} error
   * @param  {Object} obj
   *
   * @return {ServerResponse}
   */
  res.error = (error = null, obj = {}) => {
    if (process.env.NODE_ENV === 'development') {
      console.error(error)
      // TODO: Implementar aqui un sistema de logger
    }

    return res.status(500).json(
      Object.assign({ passed: false, message: 'Parece que tuvimos un error' }, obj)
    )
  }

  /**
   * Nuevos registros en el sistema.
   *
   * @param {Object} resource
   *
   * @return {ServerResponse}
   */
  res.created = (resource = {}) => {
    return res.status(201).json(resource)
  }

  /**
   * Solicitudes invalidadas por el req-validator
   *
   * @param {Array}  errors
   * @param {String} message
   *
   * @return {ServerResponse}
   */
  res.unvalidated = (errors = [], message = 'Imposible procesar la solicitud') => {
    return res.status(422).json(
      Object.assign({ passed: false, message }, { errors })
    )
  }

  /**
   * Excepción para usuarios no autenticados
   *
   * @param  {String} message
   *
   * @return {ServerResponse}
   */
  res.unauthenticated = (message = 'No estas autenticado') => {
    return res.status(401).json({ passed: false, message })
  }

  /**
   * Detener la app en procesos inesperados.
   *
   * @param  {Number} code
   * @param  {String} message
   * @param  {Object} obj
   *
   * @return {ServerResponse}
   */
  res.abort = (code = 404, message = 'Lamentablemente la app se detuvo', obj = {}) => {
    return res.status(code).json(
      Object.assign({ passed: false, message }, obj)
    )
  }

  next()
})

// Registrando los controladores de la autenticación.
authRoutes(router)
todoRoutes(router)

module.exports = router
