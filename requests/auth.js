const Validator = require('../utils/validator')

// Reglas de validación para peticiónes de login.
// @var Object
const loginRules = {
  email: { email: true, minLength: 6 },
  password: { required: true, minLength: 6 }
}

// Reglas de validación para peticiones de registro.
// @var Object
const registerRules = Object.assign({}, loginRules, {
  name: { required: true, minLength: 6 }
})

// Middleware para peticiones de login
// @param req IncomingMessage
// @param res ServerResponse
// @param next Function
//
// @return Function
module.exports.login = (req, res, next) => {
  (new Validator(req.body, loginRules)).validate(errors => {
    errors.length ? res.unvalidated(errors) : next()
  })
}

// Middleware para peticiones de registro
// @param req IncomingMessage
// @param res ServerResponse
// @param next Function
//
// @return Function
module.exports.register = async (req, res, next) => {
  const validator = new Validator(req.body, registerRules)
  // Algunas validaciones de la clase Validator son asíncronas,
  // esto limita al auto-cargador de reglas a realizar todo
  // sincrónica-mente. Debido a esto, debemos implementar un await
  // en ese tipo de funciones.
  //
  // TODO: Posible BUG en este paso.
  await validator.unique('email', 'users')
  validator.validate((errors) => {
    errors.length ? res.unvalidated(errors) : next()
  })
}
