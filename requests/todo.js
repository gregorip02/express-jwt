const Validator = require('../utils/validator')

// Reglas de validación para nuevos recursos.
//
// @var Object
const storeRules = {
  description: { required: true, minLength: 5 }
}

// Reglas de validación para actualizar recursos.
//
// @var Object
const updateRules = Object.assign({}, storeRules, {
  id: { objectId: true }
})

// Creando un nuevo recurso.
//
// @param req IncomingMessage
// @param res ServerResponse
// @param next Function
//
// @return Function
module.exports.store = (req, res, next) => {
  (new Validator(req.body, storeRules)).validate(errors => {
    errors.length ? res.unvalidated(errors) : next()
  })
}

// Obtiene detalles sobre un recurso
//
// @param req IncomingMessage
// @param res ServerResponse
// @param next Function
//
// @return Function
module.exports.show = (req, res, next) => {
  (new Validator(req.params, { id: { objectId: true } })).validate(errors => {
    errors.length ? res.unvalidated(errors) : next()
  })
}

// Actualiza un recurso
//
// @param req IncomingMessage
// @param res ServerResponse
// @param next Function
//
// @return Function
module.exports.update = (req, res, next) => {
  const data = Object.assign({}, req.params, req.body);
  (new Validator(data, updateRules)).validate(errors => {
    errors.length ? res.unvalidated(errors) : next()
  })
}
