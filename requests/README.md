# Request

Los request (peticiones), son middlewares de validación. Estos se ejecutan previo
del punto final de una petición HTTP, el objetivo de esto es validar
los datos que puedan ser enviados por el cliente.

#### Ejemplo:
  - Direcciones de correo electrónico.
  - Nombres de usuario.
  - Tokens (Autenticación).

## Implementación

Utilice requests cuando requiera que los datos recibidos en su endpoint esten previamente
validados, ademas, si desea detener la petición cuando los datos no pasen la respectiva
validación.
