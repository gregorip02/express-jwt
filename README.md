# JWT Backend

> Please contribute and help Venezuelan programmers:
> [donate $5](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=gregori.pineres02@gmail.com&item_name=Saving%20Venezuelan%20programmers&amount=5.00&currency_code=USD)

Una aplicación de Express con un sistema de autenticación basada en json web token.
Útil para hacer pruebas de autenticación con clientes que entiendan el protocolo HTTP.

## Instalación y arranque

Abre una terminal y ejecuta lo siguiente:

```bash
git clone https://github.com/gregorip02/jwt-backend
cd jwt-backend
npm install
cp .env.example .env
npm run start-dev
```
