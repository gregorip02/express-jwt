const mongoose = require('mongoose')

const TodoSchema = new mongoose.Schema({
  description: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
}, { collection: 'todos', timestamps: true })

module.exports = mongoose.model('Todo', TodoSchema)
