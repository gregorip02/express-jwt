const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('../utils/jwt')

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  todos: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Todo'
  }]
}, { collection: 'users', timestamps: true })

// La función utilizada por mongoose para exponer
// los datos del usuario.
//
// @return Object user
UserSchema.methods.toJSON = function () {
  return { name: this.name, email: this.email }
}

// Devuelve un objeto con los datos que son utilizados
// como payload por el jwt.
//
// @return Object payload
UserSchema.methods.toPayload = function () {
  return Object.assign({}, this.toJSON(), {
    signed: Date.now()
  })
}

// Hace una comprobación mediante el paquete bcrypt
// del hash almacenado en la base de datos con la contraseña
// no hasheada enviada por el usuario.
//
// En caso de un match, se resuelve una promesa con el
// token de acceso. Caso contrario, la promesa rechazada.
//
// @return Promise
UserSchema.methods.login = function (password) {
  const user = this
  return new Promise(function (resolve, reject) {
    const passed = bcrypt.compareSync(password, user.password)
    if (passed) {
      const token = jwt.sign(user.toPayload())
      resolve(token)
    } else {
      reject(passed)
    }
  })
}

// Hashea la contraseña usando el paquete bcrypt
// antes de registrar al usuario.
//
// @return Function next
UserSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = bcrypt.hashSync(this.password, 10)
  }

  next()
})

module.exports = mongoose.model('User', UserSchema)
