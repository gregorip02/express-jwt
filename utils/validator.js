const db = require('./mongodb')
const { Types } = require('mongoose')

/**
 * Una simple clase para validar los datos enviados en las peticiones HTTP.
 *
 * @author Gregori Piñeres <gregori.pineres02@gmail.com>
 * @license MIT
 */
module.exports = class Validator {
  /**
   * Instancia de la clase
   *
   * @param {Object} data
   * @param {Array} rules
   *
   * @return void
   */
  constructor (data = {}, rules = []) {
    this.data = data
    this.rules = rules
    this.errors = []
  }

  /**
   * Retorna el valor del atributo ha validar.
   *
   * @param {String} attr
   *
   * @return String || false
   */
  value (attr) {
    return this.data[attr] || false
  }

  /**
   * Parseador de strings
   *
   * @param  {String} str
   *
   * @return {String}
   */
  parseString (str) {
    return String(str).replace(/false|null|undefined/ig, '').trim()
  }

  /**
   * Agrega un error si el string dado no es un ObjectId valido
   *
   * @param  {String} attr
   * @param  {String} message
   *
   * @return void
   */
  objectId (attr, message = 'Identificador no valido') {
    const value = this.value(attr)
    if (!Types.ObjectId.isValid(value)) {
      this.errors.push({ attr, message })
    }
  }

  /**
   * Verifica que exista y retorna el atributo en el cuerpo de la petición.
   *
   * @param  {String|Array} attr
   * @param  {String} message
   *
   * @return void
   */
  required (attr, message = 'Este atributo es requerido') {
    const value = this.value(attr)
    if (!value) this.errors.push({ attr, message })
  }

  /**
   * Agrega un error si el valor esta registrado.
   *
   * @param {String} attr
   * @param {String} collection
   * @param {String} message
   */
  async unique (attr, collection, message = 'Parece que %s ya esta registrado') {
    const value = this.value(attr)
    const query = JSON.parse(`{"${attr}":"${value}"}`)
    const doc = await db.collection(collection).findOne(query)
    if (doc) {
      message = message.replace('%s', value)
      this.errors.push({ attr, message })
    }
  }

  /**
   * Comprueba que el valor enviado sea un numero.
   *
   * @param {String} attr
   *
   * @return void
   */
  numeric (attr, message = 'Debe ser númerico') {
    const value = this.value(attr)
    const number = Number(value)

    if (value && !isNaN(number)) {
      this.errors.push({ attr, message })
    }
  }

  /**
   * Comprueba que el valor enviado este dentro de los soportados.
   *
   * @param  {String} attr
   * @param  {Array}  values
   * @param  {String} message
   *
   * @return void
   */
  enum (attr, values = [], message = 'Tiene que estar entre: ') {
    const value = this.value(attr)
    if (values.indexOf(value) < 0) {
      // Concatenar los valores con comas
      // y eliminar el ultimo caracter.
      // Ejm: a, b, c, => a, b, c.
      message += values.join(', ').replace(/(,)$/, '.')
      this.errors.push({ attr, message })
    }
  }

  /**
   * Comprueba que la longitud de un atributo no sea menor que %s
   *
   * @param  {String}  attr
   * @param  {Numeric} length
   * @param  {String}  message
   *
   * @return void
   */
  minLength (attr, length, message = 'No debe tener menos de %s caracteres') {
    const value = this.value(attr)
    const string = this.parseString(value)

    if (value && string.length < length) {
      message = message.replace('%s', length)
      this.errors.push({ attr, message })
    }
  }

  /**
   * Comprueba que la longitud de un atributo no sea mayor que %s
   *
   * @param  {String}  attr
   * @param  {Numeric} length
   * @param  {String}  message
   *
   * @return void
   */
  maxLength (attr, length, message = 'No debe tener mas de %s caracteres') {
    const value = this.value(attr)
    const string = this.parseString(value)

    if (value && string.length > length) {
      message = message.replace('%s', length)
      this.errors.push({ attr, message })
    }
  }

  /**
   * Comprueba que la longitud de un atributo no sea diferente de %s
   *
   * @param  {String}  attr
   * @param  {Numeric} length
   * @param  {String}  message
   *
   * @return void
   */
  length (attr, length, message = 'Debe tener exactamente %s caracteres') {
    const value = this.value(attr)
    const string = this.parseString(value)

    if (value && string.length !== length) {
      message = message.replace('%s', length)
      this.errors.push({ attr, message })
    }
  }

  /**
   * Comprueba que el valor de un atributo no sea menor que %d
   *
   * @param  {String}  attr
   * @param  {Numeric} size
   * @param  {String}  message
   *
   * @return void
   */
  minSize (attr, size, message = 'No debe ser menor de %d') {
    const value = this.value(attr)
    const number = Number(value)

    if (value && (isNaN(number) || number < size)) {
      message = message.replace('%d', size)
      this.errors.push({ attr, message })
    }
  }

  /**
   * Comprueba que el valor de un atributo no sea mayor que %d
   *
   * @param  {String}  attr
   * @param  {Numeric} size
   * @param  {String}  message
   *
   * @return void
   */
  maxSize (attr, size, message = 'No debe ser mayor de %d') {
    const value = this.value(attr)
    const number = Number(value)

    if (value && (isNaN(number) || number > size)) {
      message = message.replace('%d', size)
      this.errors.push({ attr, message })
    }
  }

  /**
   * Comprueba que el valor enviado sea un email valido.
   *
   * @param  {String} attr
   * @param  {String} message
   *
   * @return void
   */
  email (attr, message = 'Debe ser un email valido') {
    const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    const value = this.parseString(this.value(attr))
    if (!re.test(value)) {
      this.errors.push({ attr, message })
    }
  }

  /**
   * Automatización de las validaciones.
   *
   * @param  {Function} callback
   *
   * @return {Function}
   */
  validate (callback) {
    Object.keys(this.rules).forEach(attr => {
      Object.keys(this.rules[attr]).forEach(method => {
        // Verificar si los metodos recibidos existen en esta clase.
        if (this[method] && typeof this[method] === 'function') {
          // Estos pueden ser un valor directo, o un array de valores
          // para validaciones que requieran mas de 1 argumento.
          const params = this.rules[attr][method]

          if (this[method].constructor.name === 'AsyncFunction') {
            console.info('No incluyas validaciones asyncronas en tus reglas')
          } else {
            this.call(method, attr, params)
          }
        }
      })
    })

    callback(this.errors)
  }

  /**
   * Ejecuta una llamada al método especificado.
   *
   * @param  {String} method
   * @param  {String} attr
   * @param  {Mixed}  params
   *
   * @return void
   */
  call (method, attr, params) {
    if (typeof params === 'object') {
      this[method](attr, ...params)
    } else if (typeof params === 'boolean') {
      this[method](attr)
    } else {
      this[method](attr, params)
    }
  }
}
