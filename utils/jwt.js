const jwt = require('jsonwebtoken')

// Crea un nuevo JWT a partir de un payload.
//
// @param String payload
//
// @return String token
module.exports.sign = function (payload) {
  return jwt.sign(payload, process.env.PRIVATE_KEY, {
    expiresIn: '1d'
  })
}

// Verifica la autenticidad de un token.
//
// @param String token
//
// @return Promise
module.exports.verify = function (token = 'invalid.token.send') {
  return new Promise((resolve, reject) => {
    try {
      const decode = jwt.verify(token, process.env.PRIVATE_KEY)
      resolve(decode)
    } catch (e) {
      reject(e)
    }
  })
}
