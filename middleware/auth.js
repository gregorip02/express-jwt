const jwt = require('../utils/jwt')
const User = require('../models/user')

/**
 * Verificación de token de acceso.
 *
 * @param {IncomingMessage} req
 * @param {ServerResponse} res
 * @param {Function} next
 *
 * @return {Function} next
 */
module.exports = (req, res, next) => {
  if (req.headers.authorization) {
    const token = String(req.headers.authorization)
    jwt.verify(token).then(decoded => {
      // TODO: Comprobar que el usuario exista.
      User.findOne({ email: decoded.email }).then(user => {
        if (user) {
          // El usuario es valido y es añadido al request
          req.user = user
          next()
        } else {
          // El token decodificado no coincide con
          // un usuario registrado.
          res.unauthenticated()
        }
      }).catch(err => {
        // El token decodificado no coincide con
        // un usuario registrado.
        console.log(err)
        res.unauthenticated()
      })
    }).catch((err) => {
      // El token enviando no tiene valides.
      console.error(err)
      res.unauthenticated()
    })
  } else {
    // No se ha enviando un token de autorización
    res.unauthenticated()
  }
}
